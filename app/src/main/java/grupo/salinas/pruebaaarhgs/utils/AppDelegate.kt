package grupo.salinas.pruebaaarhgs.utils

import androidx.multidex.MultiDexApplication
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.rx.RealmObservableFactory

class AppDelegate : MultiDexApplication() {
    override fun onCreate() {
        super.onCreate()
        Realm.init(this)

        val config = RealmConfiguration.Builder().name("gsdatabase.realm").schemaVersion(1)
            .allowQueriesOnUiThread(true)
            .allowWritesOnUiThread(true)
            .deleteRealmIfMigrationNeeded().rxFactory(RealmObservableFactory(false)).build()

        Realm.setDefaultConfiguration(config)
    }
}