package grupo.salinas.pruebaaarhgs.utils

import grupo.salinas.pruebaaarhgs.R
import grupo.salinas.pruebaaarhgs.models.ErrorModel

class ErrorUtils {
    fun getError(code: Int, msg: String): ErrorModel {
        val errorModel = ErrorModel()
        errorModel.errorCode = code
        errorModel.msgError = msg
        when (code) {
            7, 34 -> {
                errorModel.titleError = "¡Ups!"
                errorModel.imgError = R.drawable.ic_server_failure
                errorModel.txtButton = "Reintentar"
            }

            10 -> {
                errorModel.titleError = "¡Ups!"
                errorModel.imgError = R.drawable.ic_server_failure
                errorModel.msgError = "Para poder continuar viendo el contenido sin internet debe cargar la informacion por lo meni una vez"
                errorModel.txtButton = "Reintentar"
            }

            else -> {
                errorModel.titleError = "¡Ups!"
                errorModel.imgError = R.drawable.ic_without_internet
                errorModel.txtButton = "Seguir usando la app"
            }
        }

        return errorModel
    }
}