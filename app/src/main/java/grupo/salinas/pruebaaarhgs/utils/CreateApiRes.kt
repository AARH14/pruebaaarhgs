package grupo.salinas.pruebaaarhgs.utils

import grupo.salinas.pruebaaarhgs.interfaces.MovieMPApiService
import grupo.salinas.pruebaaarhgs.interfaces.MoviePNApiService
import grupo.salinas.pruebaaarhgs.interfaces.MovieVideoApiService
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object CreateApiRes {
    fun makeRetrofitServiceMP(): MovieMPApiService {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constans.BASE_URL)
            .build().create(MovieMPApiService::class.java)
    }

    fun makeRetrofitServicePN(): MoviePNApiService {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constans.BASE_URL)
            .build().create(MoviePNApiService::class.java)
    }

    fun makeRetrofitServiceVideo(): MovieVideoApiService {
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constans.BASE_URL)
            .build().create(MovieVideoApiService::class.java)
    }
}