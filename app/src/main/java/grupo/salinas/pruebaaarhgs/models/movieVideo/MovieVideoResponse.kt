package grupo.salinas.pruebaaarhgs.models.movieVideo

import com.google.gson.annotations.SerializedName

open class MovieVideoResponse(
    @SerializedName("results")
    open val movieVideoList: List<MovieVideoModel>
)