package grupo.salinas.pruebaaarhgs.models.movieVideo

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import java.io.Serializable

open class MovieVideoModel : RealmObject(), Serializable {
    @SerializedName("iso_639_1")
    open var iso639: String? = null

    @SerializedName("iso_3166_1")
    open var iso3166: String? = null

    @SerializedName("name")
    open var name: String? = null

    @SerializedName("key")
    open var key: String? = null

    @SerializedName("site")
    open var site: String? = null

    @SerializedName("size")
    open var size: String? = null

    @SerializedName("type")
    open var type: String? = null

    @SerializedName("official")
    open var official: String? = null

    @SerializedName("published_at")
    open var publishedAt: String? = null

    @SerializedName("id")
    open var id: String? = null
}