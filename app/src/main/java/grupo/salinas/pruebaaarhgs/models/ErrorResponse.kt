package grupo.salinas.pruebaaarhgs.models

import com.google.gson.annotations.SerializedName

data class ErrorResponse(
    @SerializedName("status_message")
    val statusMsg: String?,

    @SerializedName("status_code")
    val statusCode: Int?
)