package grupo.salinas.pruebaaarhgs.models.movieMP

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import java.io.Serializable

open class MovieMPModel : RealmObject(), Serializable {
    @SerializedName("id")
    open var id: String? = null

    @SerializedName("overview")
    open var description: String? = null

    @SerializedName("title")
    open var title: String? = null

    @SerializedName("poster_path")
    open var poster: String? = null

    @SerializedName("release_date")
    open var release: String? = null
}
