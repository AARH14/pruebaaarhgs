package grupo.salinas.pruebaaarhgs.models

data class ErrorModel(
    var errorCode: Int = 0,
    var imgError: Int = 0,
    var titleError: String = "",
    var msgError: String = "",
    var txtButton: String = ""
)