package grupo.salinas.pruebaaarhgs.models.movieMP

import com.google.gson.annotations.SerializedName

open class MovieMPResponse(
    @SerializedName("results")
    open val moviesMP: List<MovieMPModel>
)