package grupo.salinas.pruebaaarhgs.models.moviePN

import com.google.gson.annotations.SerializedName

open class MoviePNResponse (
    @SerializedName("results")
    open val moviePN: List<MoviePNModel>
)