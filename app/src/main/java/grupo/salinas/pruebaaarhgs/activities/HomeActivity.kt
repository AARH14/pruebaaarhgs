package grupo.salinas.pruebaaarhgs.activities

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import grupo.salinas.pruebaaarhgs.R
import grupo.salinas.pruebaaarhgs.adapters.MovieMPAdapter
import grupo.salinas.pruebaaarhgs.adapters.MoviePNAdapter
import grupo.salinas.pruebaaarhgs.databinding.ActivityHomeBinding
import grupo.salinas.pruebaaarhgs.models.movieMP.MovieMPModel
import grupo.salinas.pruebaaarhgs.models.moviePN.MoviePNModel
import grupo.salinas.pruebaaarhgs.utils.NetworkConnection
import grupo.salinas.pruebaaarhgs.utils.CreateApiRes
import grupo.salinas.pruebaaarhgs.utils.ErrorUtils
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding
    private lateinit var realm: Realm
    private lateinit var networkConnection: NetworkConnection

    private lateinit var moviePNAdapter: MoviePNAdapter
    private lateinit var lytMangerPN: RecyclerView.LayoutManager
    private lateinit var movieMPAdapter: MovieMPAdapter
    private lateinit var glytMangerMP: GridLayoutManager

    private var movieMPList: ArrayList<MovieMPModel> = ArrayList()
    private var moviePNList: ArrayList<MoviePNModel> = ArrayList()
    private val uiScope = CoroutineScope(Dispatchers.IO)
    private val sliderHandler = Handler(Looper.getMainLooper())
    private val sliderRunnable = Runnable {
        binding.vPMoviePN.currentItem = binding.vPMoviePN.currentItem + 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)

        realm = Realm.getDefaultInstance()

        networkConnection = NetworkConnection(this)
        setupNetworkConection()

        lytMangerPN = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        glytMangerMP = GridLayoutManager(this, 2)

        moviePNAdapter = object : MoviePNAdapter(moviePNList, binding.vPMoviePN) {
            override fun getMovie(movie: MoviePNModel) {
                val intent = Intent(this@HomeActivity, DetailMovieMPActivity::class.java)
                intent.putExtra("IdMovie", movie.id)
                startActivity(intent)
            }
        }

        movieMPAdapter = object : MovieMPAdapter(movieMPList) {
            override fun getMovie(movie: MovieMPModel) {
                val intent = Intent(this@HomeActivity, DetailMovieMPActivity::class.java)
                intent.putExtra("IdMovie", movie.id)
                startActivity(intent)
            }
        }

        with(binding) {
            swipeRefresh.setOnRefreshListener {
                swipeRefresh.isRefreshing = false
                setupNetworkConection()
            }

            vPMoviePN.adapter = moviePNAdapter
            vPMoviePN.getChildAt(0).overScrollMode = RecyclerView.OVER_SCROLL_NEVER

            vPMoviePN.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
                override fun onPageSelected(position: Int) {
                    super.onPageSelected(position)

                    sliderHandler.removeCallbacks(sliderRunnable)
                    sliderHandler.postDelayed(sliderRunnable, 2500)
                }
            })

            rvMostPopular.apply {
                layoutManager = glytMangerMP
                setHasFixedSize(true)
                adapter = movieMPAdapter
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private suspend fun parseJson() {
        withContext(Dispatchers.IO) {
            val responsePN = CreateApiRes.makeRetrofitServicePN().getMoviePN()
            val responseMP = CreateApiRes.makeRetrofitServiceMP().getMovieMP()

            if (responsePN.isSuccessful) {
                val moviePNRes = responsePN.body()!!.moviePN
                withContext(Dispatchers.Main) {
                    if (moviePNRes.isNotEmpty()) {
                        realm.beginTransaction()
                        realm.insertOrUpdate(moviePNRes)
                        realm.commitTransaction()

                        moviePNList.clear()
                        moviePNList.addAll(moviePNRes)
                        moviePNAdapter.notifyDataSetChanged()
                        binding.container.pgdWaiting.visibility = View.GONE
                    }
                }
            } else {
                withContext(Dispatchers.Main) {
                    showError(responsePN.code(), responsePN.message())
                }
            }

            if (responseMP.isSuccessful) {
                val movieRes = responseMP.body()!!.moviesMP
                withContext(Dispatchers.Main) {
                    if (movieRes.isNotEmpty()) {
                        realm.beginTransaction()
                        realm.insertOrUpdate(movieRes)
                        realm.commitTransaction()

                        movieMPList.clear()
                        movieMPList.addAll(movieRes)
                        movieMPAdapter.notifyDataSetChanged()

                        binding.container.pgdWaiting.visibility = View.GONE
                    }
                }
            } else {
                withContext(Dispatchers.Main) {
                    showError(responseMP.code(), responseMP.message())
                }
            }
        }
    }

    private fun showError(code: Int, msgError: String) {
        val error = ErrorUtils().getError(code, msgError)
        with(binding.container) {
            txtTitleError.text = error.titleError
            txtMsgError.text = error.msgError
            imgError.setImageDrawable(
                ContextCompat.getDrawable(
                    this@HomeActivity,
                    error.imgError
                )
            )

            btnAcept.text = error.txtButton

            lytError.visibility = View.VISIBLE
            pgdWaiting.visibility = View.GONE
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun connectivityMonitor(isNetworkAvailable: Boolean) {
        if (!isNetworkAvailable) {
            showError(10, getString(R.string.title_error_connetion))
        } else {
            binding.container.pgdWaiting.visibility = View.VISIBLE
            uiScope.launch { parseJson() }
        }
    }

    private fun setupNetworkConection() {
        networkConnection = NetworkConnection(this)
        networkConnection.observe(this) { isNetworkAvailable ->
            connectivityMonitor(isNetworkAvailable)
        }
    }
}