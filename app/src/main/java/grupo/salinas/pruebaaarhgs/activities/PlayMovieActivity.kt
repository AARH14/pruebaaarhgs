package grupo.salinas.pruebaaarhgs.activities

import android.os.Bundle
import android.widget.Toast
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import grupo.salinas.pruebaaarhgs.databinding.ActivityPlayMovieBinding
import grupo.salinas.pruebaaarhgs.utils.Constans
import grupo.salinas.pruebaaarhgs.utils.CreateApiRes
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class PlayMovieActivity : YouTubeBaseActivity() {
    private lateinit var binding: ActivityPlayMovieBinding

    private var id = ""
    private val uiScope = CoroutineScope(Dispatchers.IO)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPlayMovieBinding.inflate(layoutInflater)
        setContentView(binding.root)

        id = intent.getStringExtra("IdMovie") ?: ""

        uiScope.launch { getVideo() }
    }

    private suspend fun getVideo() {
        withContext(Dispatchers.IO) {
            val response = CreateApiRes.makeRetrofitServiceVideo().getMovieVideo(id)

            if (response.isSuccessful) {
                val videoRes = response.body()!!.movieVideoList

                withContext(Dispatchers.Main) {
                    val idVideo = videoRes[0].key

                    binding.youtubePlayer.initialize(
                        Constans.YOUTUBE_API_KEY,
                        object : YouTubePlayer.OnInitializedListener {
                            override fun onInitializationSuccess(
                                p0: YouTubePlayer.Provider?,
                                p1: YouTubePlayer?,
                                p2: Boolean
                            ) {
                                p1?.loadVideo(idVideo)
                                p1?.play()
                            }

                            override fun onInitializationFailure(
                                p0: YouTubePlayer.Provider?,
                                p1: YouTubeInitializationResult?
                            ) {
                                Toast.makeText(applicationContext, "Error", Toast.LENGTH_SHORT)
                                    .show()
                            }
                        })
                }
            }
        }
    }
}