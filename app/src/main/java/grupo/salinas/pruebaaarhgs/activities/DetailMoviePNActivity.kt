package grupo.salinas.pruebaaarhgs.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import grupo.salinas.pruebaaarhgs.R
import grupo.salinas.pruebaaarhgs.databinding.ActivityDetailMoviePnBinding
import grupo.salinas.pruebaaarhgs.models.movieMP.MovieMPModel
import grupo.salinas.pruebaaarhgs.utils.Constans
import io.realm.Realm
import io.realm.kotlin.where

class DetailMoviePNActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailMoviePnBinding
    private lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val idMovie = intent.getStringExtra("IdMovie") ?: ""

        if (idMovie.isNotEmpty()) {
            realm = Realm.getDefaultInstance()
            val movieRes = realm.where<MovieMPModel>().equalTo("id", idMovie).findFirst()

            if (movieRes != null) {
                val urlImage = Constans.IMAGE_BASE + movieRes.poster

                Glide.with(this)
                    .load(urlImage)
                    .centerCrop()
                    .placeholder(R.drawable.ic_logo)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(binding.imgMovie)

                binding.txtNameMovie.text = movieRes.title
                binding.txtDescriptionMovie.text = movieRes.description
            }
        }
    }
}