package grupo.salinas.pruebaaarhgs.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import grupo.salinas.pruebaaarhgs.R
import grupo.salinas.pruebaaarhgs.databinding.ItemMovieMpBinding
import grupo.salinas.pruebaaarhgs.models.movieMP.MovieMPModel
import grupo.salinas.pruebaaarhgs.utils.Constans

abstract class MovieMPAdapter(private var movieList: ArrayList<MovieMPModel>) :
    RecyclerView.Adapter<MovieMPAdapter.ViewHolder>() {
    private lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_movie_mp, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = movieList[position]
        val imgUrl = Constans.IMAGE_BASE + movie.poster
        with(holder.binding) {
            txtNameMovie.text = movie.title
            txtYear.text = movie.release

            Glide.with(mContext)
                .load(imgUrl)
                .centerCrop()
                .placeholder(R.drawable.ic_logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgMovie)

            cvMovie.setOnClickListener { getMovie(movie) }
        }
    }

    override fun getItemCount(): Int = movieList.size

    abstract fun getMovie(movie: MovieMPModel)

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemMovieMpBinding.bind(view)
    }
}