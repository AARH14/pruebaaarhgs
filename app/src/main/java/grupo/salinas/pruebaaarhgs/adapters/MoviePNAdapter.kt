package grupo.salinas.pruebaaarhgs.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import grupo.salinas.pruebaaarhgs.R
import grupo.salinas.pruebaaarhgs.databinding.ItemMoviePnBinding
import grupo.salinas.pruebaaarhgs.models.moviePN.MoviePNModel
import grupo.salinas.pruebaaarhgs.utils.Constans

abstract class MoviePNAdapter(private val moviePNList: ArrayList<MoviePNModel>, private val vPMovie: ViewPager2 ) :
    RecyclerView.Adapter<MoviePNAdapter.ViewHolder>() {
    private lateinit var mContext: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_movie_pn, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = moviePNList[position]
        val imgUrl = Constans.IMAGE_BASE + movie.poster

        with(holder.binding) {
            txtNameMovie.text = movie.title
            txtYear.text = movie.release

            Glide.with(mContext)
                .load(imgUrl)
                .centerCrop()
                .placeholder(R.drawable.ic_logo)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imgMovie)

            cvMovie.setOnClickListener { getMovie(movie) }

            if (position == moviePNList.size -2){
                vPMovie.post(runnable)
            }
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private val runnable = Runnable {
        moviePNList.addAll(moviePNList)
        notifyDataSetChanged()
    }

    abstract fun getMovie(movie: MoviePNModel)

    override fun getItemCount(): Int = moviePNList.size

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemMoviePnBinding.bind(view)
    }
}