package grupo.salinas.pruebaaarhgs.interfaces

import grupo.salinas.pruebaaarhgs.models.movieVideo.MovieVideoResponse
import grupo.salinas.pruebaaarhgs.utils.Constans
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface MovieVideoApiService {
    @GET("movie/{movieId}/videos?api_key=${Constans.API_KEY}")
    suspend fun getMovieVideo(@Path("movieId") movieId: String): Response<MovieVideoResponse>
}