package grupo.salinas.pruebaaarhgs.interfaces

import grupo.salinas.pruebaaarhgs.models.movieMP.MovieMPResponse
import grupo.salinas.pruebaaarhgs.utils.Constans
import retrofit2.Response
import retrofit2.http.GET

interface MovieMPApiService {
    @GET("movie/popular?api_key=${Constans.API_KEY}")
    suspend fun getMovieMP(): Response<MovieMPResponse>
}