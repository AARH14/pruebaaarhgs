package grupo.salinas.pruebaaarhgs.interfaces

import grupo.salinas.pruebaaarhgs.models.moviePN.MoviePNResponse
import grupo.salinas.pruebaaarhgs.utils.Constans
import retrofit2.Response
import retrofit2.http.GET

interface MoviePNApiService {
    @GET("movie/now_playing?api_key=${Constans.API_KEY}")
    suspend fun getMoviePN(): Response<MoviePNResponse>
}